package chris.sullivan.h0me;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.HashMap;

import chris.sullivan.h0me.viewloader.ResourceLoader;
import chris.sullivan.h0me.fragments.FragmentA;
import chris.sullivan.h0me.fragments.FragmentB;
import chris.sullivan.h0me.fragments.FragmentHandler;
import chris.sullivan.h0me.viewloader.Resource;

public class HomeLaunch extends ActionBarActivity {

    @Resource( id = R.id.container )
    public FrameLayout container;

    @Resource( id = R.id.text )
    public TextView howdy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_launch);

        FragmentHandler
                .getInstance( getFragmentManager() )
                .container( R.id.container )
                .add( "testa", new FragmentA() )
                .add( "testb", new FragmentB() )
                .navigateTo("testa");

        ResourceLoader.loadViews( this );

        howdy.setText( "Banana Bus" );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_launch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean addedB = false;

    @Override
    public void onBackPressed() {
        if( !addedB )
        {
            FragmentHandler
                    .getInstance( getFragmentManager() )
                    .navigateWithData( "testb", new HashMap<String, Object>(){{
                        put("Hello", 6);
                        put("World", new int[]{ 1, 2, 3, 4, 5, 6 });
                    }});
            addedB = true;
        }
        else
        {
            super.onBackPressed();
        }
    }
}
