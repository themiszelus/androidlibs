package chris.sullivan.h0me.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import chris.sullivan.h0me.R;
import chris.sullivan.h0me.viewloader.Resource;
import chris.sullivan.h0me.viewloader.ResourceLoader;

/**
 * Created by chris on 6/4/15.
 */
public class FragmentA extends Fragment {

    @Resource( id = R.id.textView )
    public TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_a, container, false );

        ResourceLoader.loadViews(this, root);

        textView.setText( "TESTING" );

        return root;
    }
}
