package chris.sullivan.h0me.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;

import chris.sullivan.h0me.R;

/**
 * Created by chris on 6/4/15.
 */
public class FragmentB extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle b = getArguments();

        int num = b.getInt( "Hello" );
        int[] arr = b.getIntArray( "World" );

        Log.d("hey", num + " " + Arrays.toString(arr));

        return inflater.inflate(R.layout.fragment_b, container, false );
    }
}
