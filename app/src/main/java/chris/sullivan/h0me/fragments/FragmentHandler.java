package chris.sullivan.h0me.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by chris on 6/4/15.
 */
public class FragmentHandler {

    private static final String TAG = "FragmentHandler";

    // view container resource id to place into
    private int container = -1;

    // fragment lookup dictionary
    private static Map<Object, Fragment> fragments;

    private FragmentManager manager;
    public static FragmentHandler getInstance( FragmentManager fMan )
    {
        if( handler == null ) handler = new FragmentHandler( fMan );

        return handler;
    }

    private static FragmentHandler handler;
    private FragmentHandler( FragmentManager fMan )
    {
        this.manager = fMan;
        this.fragments = new HashMap<>();
    }

    public FragmentHandler container( int res )
    {
        container = res;
        return handler;
    }

    public FragmentHandler add( Object key, Fragment fragment )
    {
        fragments.put( key, fragment );
        return handler;
    }

    public void navigateTo( Object key )
    {
        if( container == -1 )
        {
            Log.d( TAG, "Container view unset, assign with container( int containerResourceId )" );
        }
        else if( manager == null )
        {
            Log.d( TAG, "FragmentManager null or missing" );
        }
        else if( key == null )
        {
            Log.d( TAG, "FragmentManager cannot check null keys" );
        }
        else if( fragments.keySet().contains( key ) )
        {
            manager
                    .beginTransaction()
                    .replace( container, fragments.get( key ) )
                    .commit();
        }
        else
        {
            Log.d( TAG, "Could not find navigation path" );
        }
    }

    public void navigateWithData( Object key, Map<String, Object> args )
    {
        if( container == -1 )
        {
            Log.d( TAG, "Container view unset, assign with container( int containerResourceId )" );
        }
        else if( manager == null )
        {
            Log.d( TAG, "FragmentManager null or missing" );
        }
        else if( key == null )
        {
            Log.d( TAG, "FragmentManager cannot check null keys" );
        }
        else if( fragments.keySet().contains( key ) )
        {
            // get fragment
            Fragment frag = fragments.get(key);

            // if this is visible return
            if( frag.isVisible() ) return;

            // build arguments
            Bundle bundleData = buildArguments( frag.getArguments(), args);
            frag.setArguments( bundleData );

            manager
                    .beginTransaction()
                    .replace( container, frag )
                    .commit();
        }
        else
        {
            Log.d( TAG, "Could not find navigation path" );
        }
    }

    private Bundle buildArguments( Bundle bundle, Map<String, Object> args )
    {
        // if bundle is null create one
        bundle = bundle == null ? new Bundle() : bundle;

        Set<String> keys = args.keySet();
        for( String key : keys )
        {
            Object value = args.get( key );
            if( value instanceof Bundle )
            {
                bundle.putBundle(key, (Bundle) value);
            }
            else if( value instanceof Boolean )
            {
                bundle.putBoolean(key, (Boolean) value);
            }
            else if( value instanceof boolean[] )
            {
                bundle.putBooleanArray(key, (boolean[]) value);
            }
            else if( value instanceof Byte )
            {
                bundle.putByte(key, (Byte) value);
            }
            else if( value instanceof byte[] )
            {
                bundle.putByteArray(key, (byte[]) value);
            }
            else if( value instanceof Character )
            {
                bundle.putChar(key, (char) value);
            }
            else if( value instanceof char[] )
            {
                bundle.putCharArray(key, (char[]) value);
            }
            else if( value instanceof CharSequence )
            {
                bundle.putCharSequence(key, (CharSequence) value);
            }
            else if( value instanceof char[] )
            {
                bundle.putCharSequenceArray(key, (CharSequence[]) value);
            }
            else if( value instanceof Integer )
            {
                bundle.putInt( key, (int) value );
            }
            else if( value instanceof int[] )
            {
                bundle.putIntArray( key, (int[])value);
            }
        }
        return bundle;
    }

}
