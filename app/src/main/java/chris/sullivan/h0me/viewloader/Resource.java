package chris.sullivan.h0me.viewloader;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * ViewLoad is an annotation to set the resourceID above
 * relevant Views
 *
 * ElementType NOT SET SO THIS CAN BE USED THROUGHOUT ACTIVITY
 *
 * Created by chris on 6/7/15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Resource {

    int id() default -1;

}
