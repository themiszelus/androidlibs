package chris.sullivan.h0me.viewloader;

import android.app.Activity;
import android.app.Fragment;
import android.view.View;

import java.lang.reflect.Field;

/**
 * Created by chris on 6/7/15.
 */
public class ResourceLoader {

    /**
     *
     * Dynamically loads variables at RUNTIME
     *
     * @param act The activity to load views onto
     */
    public static void loadViews( Activity act )
    {
        // get activities class
        Class objClass = act.getClass();

        // get classes field variables
        Field[] fields = objClass.getDeclaredFields();
        for( Field field : fields )
        {
            // get Resource annotation if it exists
            Resource load = null;
            if( (load = field.getAnnotation(Resource.class) ) != null )
            {
                // if resource is default -1, skip this lookup
                if( load.id() == -1 ) continue;

                // set field accessible and set field
                try {
                    field.setAccessible( true );
                    field.set( act, act.findViewById( load.id() ) );
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * LoadViews during RUNTIME from the called Fragment and parent view
     *
     * @param frag The Fragment to load views onto
     * @param root The Root view to lookup Views from
     */
    public static void loadViews( Fragment frag, View root )
    {
        // get activities class
        Class objClass = frag.getClass();

        // get classes field variables
        Field[] fields = objClass.getDeclaredFields();
        for( Field field : fields )
        {
            // get Resource annotation if it exists
            Resource load = null;
            if( (load = field.getAnnotation(Resource.class) ) != null )
            {
                // if resource is default -1, skip this lookup
                if( load.id() == -1 ) continue;

                // set field accessible and set field
                try {
                    field.setAccessible( true );
                    field.set( frag, root.findViewById( load.id() ) );
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
